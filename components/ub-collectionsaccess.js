import React from 'react';
import {useState} from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';

export default function Collectionsaccess() {
  const [modalShow, setModalShow] = React.useState(false);

  return (
    <>
    <main className="container bg-light p-3 shadow mb-3">
        <div className="d-flex justify-content-between flex-wrap
                        flex-md-nowrap align-items-center pt-3 pb-2 mb-3
                        border-bottom">
            <h4 className="h4">Collections Access</h4>
        </div>
        {/* <h3>Section title</h3> */}
        <div>
            <div className="row">
            <div className="col-md-4 com-sm-12">
                <form action>
                <label htmlFor="exampleFormControlInput1" className="form-label fw-bold animate__fadeIn">Collection
                    Name</label>
                <select className="form-select form-select-sm" aria-label=".form-select-sm example">
                    <option selected>Default</option>
                    <option value={1}>One</option>
                    <option value={2}>Two</option>
                </select>
                <div className="d-flex flex-row mt-3">
                    <label htmlFor="exampleFormControlInput1" className="form-label fw-bold mb-0">Access Status</label>
                    <div className="form-check form-switch ms-3">
                    <input className="form-check-input" type="checkbox" role="switch" id="flexSwitchCheckChecked" defaultChecked />
                    </div>
                </div>
                <label htmlFor="FormControlInput1" className="form-label fw-bold mt-3">Add Tokens</label>
                <div className="d-flex flex-row">
                    <div className="mb-3">
                    <input type="text" className="form-control form-control-sm" id="FormControlInput1" placeholder />
                    </div>
                    <div>
                    <button type="button" className="btn btn-info fw-bold ms-3 btn-sm">+</button>
                    </div>
                </div>
                <button 
                    type="button" 
                    className="btn btn-primary mt-4 btn-sm fw-bold" 
                    onClick={() => setModalShow(true)}
                    >
                    Update
                    </button>
                    <Popupmodal
                        show={modalShow}
                        onHide={() => setModalShow(false)}
                    />
                </form>
            </div>
            </div>
        </div>
        </main>
    </>
  );
}
function Popupmodal(props) {
        return (
          <Modal
            {...props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
          >
            <Modal.Header closeButton>
              <Modal.Title id="contained-modal-title-vcenter">
                Collection
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <p>
                Are you sure, you want to update the Collection?
              </p>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={props.onHide}>Cancel</Button>
              <Button className='primary'>Confirm</Button>
            </Modal.Footer>
          </Modal>
        );
      }
      
