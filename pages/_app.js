// import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import '../public/css/style.css';
import '../public/fonts/stylesheet.css';
import Layout from '../components/layout/Layout';


function MyApp({ Component, pageProps }) {
  return(
    <>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </>
  );
}

export default MyApp
